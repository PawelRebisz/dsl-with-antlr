package pl.niezawodnykod.dsl.grammar;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class ConditionFactoryTest {

    private final static List<UnderTestA> SAMPLE = Arrays.asList(
            new UnderTestA("a", 1, new UnderTestB(1, new UnderTestC("a"))),
            new UnderTestA(null, 2, new UnderTestB(2, new UnderTestC("a"))),
            new UnderTestA("b", 3, new UnderTestB(3, new UnderTestC("b"))),
            new UnderTestA(null, 4, new UnderTestB(4, new UnderTestC("b"))),
            new UnderTestA("c", 5, new UnderTestB(5, new UnderTestC("c")))
    );

    private final Compiler compiler = ConditionFactory.instance();

    @Test
    void shouldNotFilterAnyObjectsForMissingExpression() {
        // when
        List<UnderTestA> results = applyConditionTo(UnderTestA.class, SAMPLE, null);

        // then
        assertThat(results).hasSize(5);
    }

    @Test
    void shouldNotFilterAnyObjectsForBlankExpression() {
        // when
        List<UnderTestA> results = applyConditionTo(UnderTestA.class, SAMPLE, "   ");

        // then
        assertThat(results).hasSize(5);
    }

    @ParameterizedTest(name = "Expecting {1} entries for expression: {0}")
    @MethodSource("equalExpressionsSample")
    void shouldFilterValuesWithEqualExpressions(String expression, int expectedNumberOfEntries) {
        // when
        List<UnderTestA> results = applyConditionTo(UnderTestA.class, SAMPLE, expression);

        // then
        assertThat(results).hasSize(expectedNumberOfEntries);
    }

    @ParameterizedTest(name = "Expecting {1} entries for expression: {0}")
    @MethodSource("differentExpressionsSample")
    void shouldFilterValuesWithDifferentExpressions(String expression, int expectedNumberOfEntries) {
        // when
        List<UnderTestA> results = applyConditionTo(UnderTestA.class, SAMPLE, expression);

        // then
        assertThat(results).hasSize(expectedNumberOfEntries);
    }

    @ParameterizedTest(name = "Expecting {1} entries for expression: {0}")
    @MethodSource("afterAndBeforeExpressionsSample")
    void shouldFilterValuesWithAfterAndBeforeExpressions(String expression, int expectedNumberOfEntries) {
        // when
        List<UnderTestA> results = applyConditionTo(UnderTestA.class, SAMPLE, expression);

        // then
        assertThat(results).hasSize(expectedNumberOfEntries);
    }

    @ParameterizedTest(name = "Expecting {1} entries for expression: {0}")
    @MethodSource("logicalExpressionsSample")
    void shouldFilterValuesWithLogicalExpressions(String expression, int expectedNumberOfEntries) {
        // when
        List<UnderTestA> results = applyConditionTo(UnderTestA.class, SAMPLE, expression);

        // then
        assertThat(results).hasSize(expectedNumberOfEntries);
    }

    @ParameterizedTest(name = "Expecting {1} entries for expression: {0}")
    @MethodSource("complexExpressionsSample")
    void shouldFilterWithComplexExpressions(String expression, int expectedNumberOfEntries) {
        // when
        List<UnderTestA> results = applyConditionTo(UnderTestA.class, SAMPLE, expression);

        // then
        assertThat(results).hasSize(expectedNumberOfEntries);
    }

    private <T> List<T> applyConditionTo(Class<T> clazz, List<T> elements, String expression) {
        Condition<T> condition = compiler.compile(clazz, expression);
        return elements.stream()
                .filter(condition::evaluate)
                .collect(Collectors.toList());
    }

    @Data
    @AllArgsConstructor
    static class UnderTestA {
        private final String name;
        private final int value;
        private final UnderTestB underTestB;
    }

    @Data
    @AllArgsConstructor
    static class UnderTestB {
        private final int value;
        private final UnderTestC underTestC;
    }

    @Data
    @AllArgsConstructor
    static class UnderTestC {
        private final String name;
    }

    private static Stream<Arguments> equalExpressionsSample() {
        return Stream.of(
                Arguments.of("undertesta.name equal 'a'", 1),
                Arguments.of("undertesta.value equal 5", 1),
                Arguments.of("undertesta.name equal null", 2)
        );
    }

    private static Stream<Arguments> differentExpressionsSample() {
        return Stream.of(
                Arguments.of("undertesta.name different 'a'", 4),
                Arguments.of("undertesta.value different 5", 4),
                Arguments.of("undertesta.name different null", 3)
        );
    }

    private static Stream<Arguments> afterAndBeforeExpressionsSample() {
        return Stream.of(
                Arguments.of("undertesta.name after 'a'", 2),
                Arguments.of("undertesta.name before 'c'", 4),
                Arguments.of("undertesta.value after 1", 4),
                Arguments.of("undertesta.value before 5", 4),
                Arguments.of("undertesta.name after null", 3),
                Arguments.of("undertesta.name before null", 0)
        );
    }

    private static Stream<Arguments> logicalExpressionsSample() {
        return Stream.of(
                Arguments.of("undertesta.name equal 'a' or undertesta.name equal 'b'", 2),
                Arguments.of("undertesta.name equal null and undertesta.value after 3", 1)
        );
    }

    private static Stream<Arguments> complexExpressionsSample() {
        return Stream.of(
                Arguments.of("( undertesta.name in ('a', 'c', 'b') or undertesta.underTestB.value after 2) ", 4),
                Arguments.of("(" +
                        "( undertesta.name in ('a', 'c', 'b') or undertesta.underTestB.value after 2) " +
                        "and " +
                        "( undertesta.underTestB.underTestC.name in ('a', 'c')) " +
                        ")", 2)
        );
    }
}