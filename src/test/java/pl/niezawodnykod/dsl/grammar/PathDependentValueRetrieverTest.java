package pl.niezawodnykod.dsl.grammar;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class PathDependentValueRetrieverTest {

    private static final UnderTestB NO_UNDER_TEST_B = null;
    private static final UnderTestC NO_UNDER_TEST_C = null;

    @Test
    void shouldNotCreateRetrieverForNotProvidedPath() {
        // expect
        assertThatThrownBy(() -> new PathDependentValueRetriever(UnderTestA.class, null))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Failed to create value retriever for class 'UnderTestA'. Missing path: 'null'.");
    }

    @Test
    void shouldNotCreateRetrieverForEmptyPath() {
        // expect
        assertThatThrownBy(() -> new PathDependentValueRetriever(UnderTestA.class, ""))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Failed to create value retriever for class 'UnderTestA'. Missing path: ''.");
    }

    @Test
    void shouldNotCreateRetrieverForPathStartingWithDifferentClassThanExpectedType() {
        // expect
        assertThatThrownBy(() -> new PathDependentValueRetriever(UnderTestA.class, "undertestb.value"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Failed to create value retriever for class 'UnderTestA'."
                        + " Path 'undertestb.value' did not start with expected class.");
    }

    @Test
    void shouldNotCreateRetrieverForPathWithoutValue() {
        // expect
        assertThatThrownBy(() -> new PathDependentValueRetriever<>(UnderTestA.class, "undertesta"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Failed to create value retriever for class 'UnderTestA'."
                        + " Path 'undertesta' did not specify value.");
    }

    @Test
    void shouldRetrieveStringValue() throws Exception {
        // given
        PathDependentValueRetriever<UnderTestA> retriever = new PathDependentValueRetriever<>(UnderTestA.class, "undertesta.name");

        // when
        Value value = retriever.getValueFrom(new UnderTestA("super mario", NO_UNDER_TEST_B));

        // then
        assertThat(value).isEqualTo(new NonNullValue("super mario"));
    }

    @Test
    void shouldRetrieveIntValue() throws Exception {
        // given
        PathDependentValueRetriever<UnderTestB> retriever = new PathDependentValueRetriever<>(UnderTestB.class, "undertestb.value");

        // when
        Value value = retriever.getValueFrom(new UnderTestB(42, NO_UNDER_TEST_C));

        // then
        assertThat(value).isEqualTo(new NonNullValue(new BigDecimal(42)));
    }

    @Test
    void shouldRetrieveValueFromNestedObject() throws Exception {
        // given
        PathDependentValueRetriever<UnderTestA> retriever = new PathDependentValueRetriever<>(
                UnderTestA.class, "undertesta.underTestB.underTestC.name");

        // when
        Value value = retriever.getValueFrom(new UnderTestA("A Team", new UnderTestB(42, new UnderTestC("C Team"))));

        // then
        assertThat(value).isEqualTo(new NonNullValue("C Team"));
    }

    @Test
    void shouldRetrieveNullValueWhenStumbledUponDuringMovingThroughThePath() throws Exception {
        // given
        PathDependentValueRetriever<UnderTestA> retriever = new PathDependentValueRetriever<>(
                UnderTestA.class, "undertesta.underTestB.underTestC.name");

        // when
        Value value = retriever.getValueFrom(new UnderTestA("A Team", new UnderTestB(42, NO_UNDER_TEST_C)));

        // then
        assertThat(value).isEqualTo(NullValue.nullValue());
    }

    @Data
    @AllArgsConstructor
    static class UnderTestA {
        private final String name;
        private final UnderTestB underTestB;
    }

    @Data
    @AllArgsConstructor
    static class UnderTestB {
        private final int value;
        private final UnderTestC underTestC;
    }

    @Data
    @AllArgsConstructor
    static class UnderTestC {
        private final String name;
    }
}