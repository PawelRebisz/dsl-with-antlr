package pl.niezawodnykod.dsl.grammar;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class CapturedTokenToValueConverter {

    private static final String NULL_KEYWORD = "null";
    private static final Pattern STRING_LITERAL_PATTERN = Pattern.compile("'(.*?)'");

    public Value convert(String value) {
        if (Objects.equals(NULL_KEYWORD, value)) {
            return NullValue.nullValue();
        }

        Matcher matcher = STRING_LITERAL_PATTERN.matcher(value);
        if (matcher.matches()) {
            return new NonNullValue(matcher.toMatchResult().group(1));
        }

        return new NonNullValue(new BigDecimal(value));
    }
}
