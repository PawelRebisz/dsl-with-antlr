package pl.niezawodnykod.dsl.grammar;

public interface Condition<T> {
    boolean evaluate(T object);
}
