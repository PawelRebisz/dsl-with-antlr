package pl.niezawodnykod.dsl.grammar;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class NotCondition<T> implements Condition<T> {

    private final Condition<T> condition;

    @Override
    public boolean evaluate(T object) {
        return !condition.evaluate(object);
    }
}
