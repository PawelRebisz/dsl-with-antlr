package pl.niezawodnykod.dsl.grammar;

import lombok.EqualsAndHashCode;

import static java.util.Objects.isNull;

@EqualsAndHashCode
public class NonNullValue implements Value {

    private final Comparable<Object> reference;

    NonNullValue(Comparable reference) {
        if (isNull(reference)) {
            throw new IllegalArgumentException("Failed to create non null container, 'null' value was passed");
        }
        this.reference = reference;
    }

    @Override
    public int compareWithInternals(Value value) {
        if (value instanceof NonNullValue) {
            NonNullValue nonNullValue = (NonNullValue) value;
            return reference.compareTo(nonNullValue.reference);

        } else if (value instanceof NullValue) {
            return HIGHER_THAN_NULL_VALUE;

        } else {
            throw new ClassCastException("Failed to compare 'non null' value with "
                    + value.getClass().getName() + ". Class types are different");
        }
    }

    @Override
    public String toString() {
        return "NonNull{" + reference + "}";
    }
}
