package pl.niezawodnykod.dsl.grammar;

public interface Value {
    int SAME_INSTANCE = 0;
    int LOWER_THAN_NON_NULL_VALUE = -1;
    int HIGHER_THAN_NULL_VALUE = 1;

    int compareWithInternals(Value value);
}
