package pl.niezawodnykod.dsl.grammar;

import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Objects;

import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static pl.niezawodnykod.dsl.grammar.NullValue.nullValue;

public class PathDependentValueRetriever<T> {
    private static final String PROPERTY_SEPARATOR = "\\.";

    private final Class<T> clazz;
    private final String[] pieces;

    public PathDependentValueRetriever(Class<T> clazz, String path) {
        if (isBlank(path)) {
            throw new IllegalArgumentException("Failed to create value retriever for class '"
                    + clazz.getSimpleName() + "'. Missing path: '" + path + "'.");
        }

        String expectedFirstPathElement = clazz.getSimpleName().toLowerCase();
        String[] pieces = path.split(PROPERTY_SEPARATOR);

        if (!Objects.equals(expectedFirstPathElement, pieces[0].toLowerCase())) {
            throw new IllegalArgumentException("Failed to create value retriever for class '" + clazz.getSimpleName()
                    + "'. Path '" + path + "' did not start with expected class.");
        }
        if (pieces.length < 2) {
            throw new IllegalArgumentException("Failed to create value retriever for class '" + clazz.getSimpleName()
                    + "'. Path '" + path + "' did not specify value.");
        }

        this.clazz = clazz;
        this.pieces = pieces;
    }

    Value getValueFrom(T object) throws Exception {
        Object currentObject = object;
        for (int index = 1; index < pieces.length; index++) {
            String nextProperty = pieces[index];
            Field field = FieldUtils.getField(currentObject.getClass(), nextProperty, true);
            currentObject = field.get(currentObject);
            if (isNull(currentObject)) {
                return nullValue();
            }
        }
        currentObject = wrapNumericValueWithBigDecimal(currentObject);
        return new NonNullValue((Comparable) currentObject);
    }

    private static Object wrapNumericValueWithBigDecimal(Object object) {
        if (Number.class.isAssignableFrom(object.getClass())) {
            return new BigDecimal(object.toString());
        }
        return object;
    }
}
