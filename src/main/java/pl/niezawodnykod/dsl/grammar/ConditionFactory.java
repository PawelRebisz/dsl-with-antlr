package pl.niezawodnykod.dsl.grammar;

import lombok.extern.log4j.Log4j2;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import pl.niezawodnykod.dsl.grammar.generated.DslGrammarLexer;
import pl.niezawodnykod.dsl.grammar.generated.DslGrammarParser;

import java.util.EnumSet;
import java.util.List;
import java.util.Objects;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static pl.niezawodnykod.dsl.grammar.ComparingCondition.Outcome.EQUAL;
import static pl.niezawodnykod.dsl.grammar.ComparingCondition.Outcome.HIGHER;
import static pl.niezawodnykod.dsl.grammar.ComparingCondition.Outcome.LOWER;

@Log4j2
public class ConditionFactory implements Compiler {

    private static final Condition ALWAYS_TRUE_CONDITION = object -> true;
    private static final Condition ALWAYS_FALSE_CONDITION = object -> false;

    private final CapturedTokenToValueConverter converter = new CapturedTokenToValueConverter();

    private ConditionFactory() { }

    public static ConditionFactory instance() {
        return new ConditionFactory();
    }

    @Override
    public <T> Condition<T> compile(Class<T> outputType, String expressionToCompile) {
        log.info("Compiling expression: {} for class of type: {}", expressionToCompile, outputType.getSimpleName());
        if (isBlank(expressionToCompile)) {
            return ALWAYS_TRUE_CONDITION;
        }
        DslGrammarLexer lexer = new DslGrammarLexer(CharStreams.fromString(expressionToCompile));
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        DslGrammarParser parser = new DslGrammarParser(tokenStream);

        ParseTreeWalker walker = new ParseTreeWalker();
        TokenToConditionListener<T> listener = new TokenToConditionListener<>(outputType, this);
        walker.walk(listener, parser.root());
        return listener.root();
    }

    public <T> Condition<T> typedCondition(Class<T> clazz, String path, String operator, String referenceValue) {
        try {
            SingleConditionFactory<T> factory = conditionFactoryFor(operator);
            PathDependentValueRetriever<T> accessor = new PathDependentValueRetriever<>(clazz, path);
            Value comparableValue = converter.convert(referenceValue);
            return factory.create(accessor, referenceValue, comparableValue);
        } catch (Exception exception) {
            throw new IllegalStateException("Failed to construct condition from path: " + path
                    + " and operator: " + operator
                    + " and value: " + referenceValue + ".", exception);
        }
    }

    private static <T> SingleConditionFactory<T> conditionFactoryFor(String operator) {
        switch (operator) {
            case "equal":
                return (accessor, referenceValue, comparableValue) ->
                        new ComparingCondition<>(EnumSet.of(EQUAL), accessor, comparableValue);
            case "different":
                return (accessor, referenceValue, comparableValue) ->
                        new ComparingCondition<>(EnumSet.of(LOWER, HIGHER), accessor, comparableValue);
            case "before":
                return (accessor, referenceValue, comparableValue) ->
                        new ComparingCondition<>(EnumSet.of(LOWER), accessor, comparableValue);
            case "after":
                return (accessor, referenceValue, comparableValue) ->
                        new ComparingCondition<>(EnumSet.of(HIGHER), accessor, comparableValue);
            default:
                throw new IllegalArgumentException("Failed to find condition factory for operator: " + operator);
        }
    }

    public <T> Condition<T> negate(String operator, Condition<T> conditionToNegate) {
        if (Objects.equals("not", operator)) {
            throw new IllegalStateException("Failed to recognize operator: " + operator + ".");
        }
        return new NotCondition<>(conditionToNegate);
    }

    public <T> Condition<T> aggregate(String operator, List<Condition<T>> conditions) {
        switch (operator) {
            case "and":
                return new AndCondition<>(conditions);
            case "or":
                return new OrCondition<>(conditions);
            case "in":
                if (conditions.isEmpty()) {
                    return ALWAYS_FALSE_CONDITION;
                }
                return new OrCondition<>(conditions);
            default:
                throw new IllegalStateException("Failed to recognize operator: " + operator + ".");
        }
    }

    @FunctionalInterface
    private interface SingleConditionFactory<T> {
        Condition<T> create(PathDependentValueRetriever<T> accessor, Object referenceValue, Value comparableValue);
    }
}
