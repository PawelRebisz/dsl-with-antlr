package pl.niezawodnykod.dsl.grammar;

import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class AndCondition<T> implements Condition<T> {

    private final List<Condition<T>> conditions;

    @Override
    public boolean evaluate(T object) {
        return conditions
                .stream()
                .allMatch(condition -> condition.evaluate(object));
    }
}
