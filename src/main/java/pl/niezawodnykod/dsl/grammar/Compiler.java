package pl.niezawodnykod.dsl.grammar;

public interface Compiler {
    <T> Condition<T> compile(Class<T> outputType, String expressionToCompile);
}
