package pl.niezawodnykod.dsl.grammar;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class NullValue implements Value {

    private static final NullValue NULL_VALUE = new NullValue();

    private NullValue() { }

    public static NullValue nullValue() {
        return NULL_VALUE;
    }

    @Override
    public int compareWithInternals(Value value) {
        if (value == NULL_VALUE) {
            return SAME_INSTANCE;

        } else if (value instanceof NonNullValue) {
            return LOWER_THAN_NON_NULL_VALUE;

        } else {
            throw new ClassCastException("Failed to compare 'null' value with "
                    + value.getClass().getName() + ". Class types are different");
        }
    }

    @Override
    public String toString() {
        return "Null{}";
    }
}
