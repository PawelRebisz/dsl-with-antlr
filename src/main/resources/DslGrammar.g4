grammar DslGrammar;

@header {
package pl.niezawodnykod.dsl.grammar.generated;

import pl.niezawodnykod.dsl.grammar.generated.DslGrammarBaseListener;
import pl.niezawodnykod.dsl.grammar.generated.DslGrammarLexer;
import pl.niezawodnykod.dsl.grammar.generated.DslGrammarListener;
import pl.niezawodnykod.dsl.grammar.generated.DslGrammarParser;
import pl.niezawodnykod.dsl.grammar.generated.DslGrammarVisitor;
}

root
    : ( expression )+ EOF
    ;

expression : NOT_OPERATOR expression
    | expression AND_OPERATOR expression
    | expression OR_OPERATOR expression
    | '(' expression ')'
    | single_condition
    ;

single_condition
    : variable ( BINARY_OPERATOR VALUE | IN_OPERATOR '(' values_in_brackets* ')' )
    ;

variable
    : VARIABLE
    ;

values_in_brackets
    : ','? VALUE
    ;

BINARY_OPERATOR
    : EQUAL_OPERATOR
    | DIFFERENT_OPERATOR
    | AFTER_OPERATOR
    | BEFORE_OPERATOR
    ;

EQUAL_OPERATOR: E Q U A L ;
DIFFERENT_OPERATOR: D I F F E R E N T ;
AFTER_OPERATOR: A F T E R ;
BEFORE_OPERATOR: B E F O R E ;

NOT_OPERATOR: N O T ;
AND_OPERATOR: A N D ;
OR_OPERATOR: O R ;
IN_OPERATOR: I N ;

VALUE
    : NULL_VALUE
    | DIGIT+
    | '\'' ALLOWED_VALUE_CHARACTER* '\''
    ;
VARIABLE
    : LETTER ALLOWED_VARIABLE_CHARACTER*
    ;
NULL_VALUE: N U L L ;

ALLOWED_VARIABLE_CHARACTER : LETTER | DIGIT | SAFE_SPECIAL_CHARACTER ;
ALLOWED_VALUE_CHARACTER : LETTER | DIGIT | SAFE_SPECIAL_CHARACTER ;
/* All whitespaces (with occurences 1..N) should be ignored */
WHITESPACE: ( ' ' | '\t' | '\r' | '\n' )+ -> skip ;

fragment SAFE_SPECIAL_CHARACTER : ( '.' | '_' | '-' | '|' | ',' ) ;
fragment LETTER: [A-Za-z] ;
fragment DIGIT: [0-9] ;

fragment A: [aA] ;
fragment B: [bB] ;
fragment C: [cC] ;
fragment D: [dD] ;
fragment E: [eE] ;
fragment F: [fF] ;
fragment G: [gG] ;
fragment H: [hH] ;
fragment I: [iI] ;
fragment J: [jJ] ;
fragment K: [kK] ;
fragment L: [lL] ;
fragment M: [mM] ;
fragment N: [nN] ;
fragment O: [oO] ;
fragment P: [pP] ;
fragment Q: [qQ] ;
fragment R: [rR] ;
fragment S: [sS] ;
fragment T: [tT] ;
fragment U: [uU] ;
fragment V: [vV] ;
fragment W: [wW] ;
fragment X: [xX] ;
fragment Y: [yY] ;
fragment Z: [zZ] ;