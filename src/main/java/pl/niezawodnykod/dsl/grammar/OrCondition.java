package pl.niezawodnykod.dsl.grammar;

import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class OrCondition<T> implements Condition<T> {

    private final List<Condition<T>> conditions;

    @Override
    public boolean evaluate(T object) {
        return conditions
                .stream()
                .anyMatch(condition -> condition.evaluate(object));
    }
}
