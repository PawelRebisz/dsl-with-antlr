package pl.niezawodnykod.dsl.grammar;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import pl.niezawodnykod.dsl.grammar.generated.DslGrammarBaseListener;
import pl.niezawodnykod.dsl.grammar.generated.DslGrammarParser;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
@RequiredArgsConstructor
public class TokenToConditionListener<T> extends DslGrammarBaseListener {

    private static final List<String> TERMINAL_NODES_TO_FILTER = Arrays.asList("(", ")", "<EOF>");

    private final Deque<String> storedValuesForGrouping = new LinkedList<>();
    private final Deque<String> errors = new LinkedList<>();
    private final Deque<Condition<T>> storedConditions = new LinkedList<>();
    private final Class<T> outputType;
    private final ConditionFactory conditionFactory;

    public Condition<T> root() {
        if (!errors.isEmpty()) {
            log.warn("Failed to create a valid condition due to errors: {}", errors);
        }
        return storedConditions.pop();
    }

    @Override
    public void exitRoot(DslGrammarParser.RootContext context) {
        super.exitRoot(context);

        process(context);
    }

    @Override
    public void exitExpression(DslGrammarParser.ExpressionContext context) {
        super.exitExpression(context);

        process(context);
    }

    @Override
    public void exitSingle_condition(DslGrammarParser.Single_conditionContext context) {
        super.exitSingle_condition(context);

        process(context);
    }

    @Override
    public void exitValues_in_brackets(DslGrammarParser.Values_in_bracketsContext context) {
        super.exitValues_in_brackets(context);

        process(context);
    }

    @Override
    public void visitErrorNode(ErrorNode errorNode) {
        super.visitErrorNode(errorNode);

        registerError(errorNode.getParent().getText(), "Problematic value is: " + errorNode.getText());
    }

    private void process(DslGrammarParser.RootContext context) {
        recordErrorsWithContext(context.getText(), () -> {
            List<ParseTree> filteredChildren = context.children
                    .stream()
                    .filter(parseTree -> !TERMINAL_NODES_TO_FILTER.contains(parseTree.getText()))
                    .collect(Collectors.toList());

            if (notAllConditionsOrValuesAreProcessed(filteredChildren)) {
                registerError("General", "It seems not all elements were correctly processed");
            }
        });
    }

    private void process(DslGrammarParser.ExpressionContext context) {
        recordErrorsWithContext(context.getText(), () -> {
            List<ParseTree> filteredChildren = context.children
                    .stream()
                    .filter(parseTree -> !TERMINAL_NODES_TO_FILTER.contains(parseTree.getText()))
                    .collect(Collectors.toList());
            switch (filteredChildren.size()) {
                case 1: // regular case
                    break;

                case 2: // this is not case
                    String notOperator = filteredChildren.get(0)
                            .getText()
                            .toLowerCase().trim();
                    Condition<T> conditionToNegate = storedConditions.pop();
                    Condition<T> negatedCondition = conditionFactory.negate(notOperator, conditionToNegate);
                    storedConditions.push(negatedCondition);
                    break;

                case 3: // this is aggregated case
                    Condition<T> childOneCondition = storedConditions.pop();
                    String operator = filteredChildren.get(1)
                            .getText()
                            .toLowerCase().trim();
                    Condition<T> childTwoCondition = storedConditions.pop();
                    Condition<T> aggregatedCondition = conditionFactory
                            .aggregate(operator, Arrays.asList(childOneCondition, childTwoCondition));
                    storedConditions.push(aggregatedCondition);
                    break;

                default:
                    throw new IllegalStateException("For some unknown reason node: " + context.getText()
                            + " has incorrect number of children.");
            }
        });
    }

    private void process(DslGrammarParser.Single_conditionContext context) {
        recordErrorsWithContext(context.getText(), () -> {
            String path = context.children.get(0).getText();
            String operator = context.children.get(1).getText().toLowerCase().trim();

            if (Objects.equals(operator, "in")) {
                Condition<T> condition = conditionFactory
                        .aggregate(operator, createEqualConditionFromStoredValues(path));
                storedConditions.push(condition);
            } else {
                String value = context.children.get(2).getText();
                Condition<T> condition = conditionFactory.typedCondition(outputType, path, operator, value);
                storedConditions.push(condition);
            }
        });
    }

    private void process(DslGrammarParser.Values_in_bracketsContext context) {
        recordErrorsWithContext(context.getText(), () -> {
            ParseTree parseTree = context.children.get(context.children.size() - 1);
            String value = parseTree.getText();
            storedValuesForGrouping.push(value);
        });
    }

    private List<Condition<T>> createEqualConditionFromStoredValues(String path) {
        List<Condition<T>> conditions = new LinkedList<>();
        while (!storedValuesForGrouping.isEmpty()) {
            String value = storedValuesForGrouping.pop();
            Condition<T> condition = conditionFactory.typedCondition(outputType, path, "equal", value);
            conditions.add(condition);
        }
        return conditions;
    }

    private void recordErrorsWithContext(String contextName, Runnable action) {
        try {
            action.run();
        } catch (Exception exception) {
            log.error("Error occurred during compilation of condition", exception);
            registerError(contextName, exception.getMessage());
        }
    }

    private void registerError(String context, String message) {
        errors.add(String.format("Error within context: %s with message: %s", context, message));
    }

    private boolean notAllConditionsOrValuesAreProcessed(List<ParseTree> filteredChildren) {
        return filteredChildren.size() != 1
                || storedConditions.size() != 1
                || storedValuesForGrouping.isEmpty();
    }
}
