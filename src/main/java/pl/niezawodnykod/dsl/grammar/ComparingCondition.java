package pl.niezawodnykod.dsl.grammar;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.util.Set;

@Log4j2
@RequiredArgsConstructor
public class ComparingCondition<T> implements Condition<T> {

    @Getter
    @AllArgsConstructor
    public enum Outcome {
        EQUAL(0),
        HIGHER(1),
        LOWER(-1);

        private int outcomeAsInt;
    }

    // Provided set of allowed outcomes is not required at this stage but if we would aim to support operators
    // like 'equal or lower' then it would prove helpful
    private final Set<Outcome> allowedOutcomes;
    private final PathDependentValueRetriever<T> accessor;
    private final Value value;

    @Override
    public boolean evaluate(T object) {
        Value valueFromObject;
        try {
            valueFromObject = accessor.getValueFrom(object);
        } catch (Exception exception) {
            log.error("Failed to obtain value from object: {}.", object, exception);
            return false;
        }
        int referenceToObjectOutcome = value.compareWithInternals(valueFromObject);
        int objectToReferenceOutcome = invertToMatchInput(referenceToObjectOutcome);
        Outcome currentOutcome = ensureWithinBounds(objectToReferenceOutcome);
        return allowedOutcomes.contains(currentOutcome);
    }

    // Inversion is required as we compare not 'object' to 'reference' but the opposite for simplicity
    private static int invertToMatchInput(int outcome) {
        return outcome * -1;
    }

    private static Outcome ensureWithinBounds(int outcome) {
        if (outcome < Outcome.EQUAL.outcomeAsInt) {
            return Outcome.LOWER;
        } else if (outcome > Outcome.EQUAL.outcomeAsInt) {
            return Outcome.HIGHER;
        }
        return Outcome.EQUAL;
    }
}
