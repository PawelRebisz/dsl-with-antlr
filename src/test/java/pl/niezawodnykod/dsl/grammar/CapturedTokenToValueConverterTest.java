package pl.niezawodnykod.dsl.grammar;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class CapturedTokenToValueConverterTest {

    private final CapturedTokenToValueConverter converter = new CapturedTokenToValueConverter();

    @Test
    void shouldConvertNullValue() {
        // when
        Value value = converter.convert("null");

        // then
        assertThat(value)
                .isInstanceOf(NullValue.class)
                .isEqualTo(NullValue.nullValue());
    }

    @Test
    void shouldConvertDecimalValue() {
        // when
        Value value = converter.convert("5");

        // then
        assertThat(value)
                .isInstanceOf(NonNullValue.class)
                .isEqualTo(new NonNullValue(new BigDecimal(5)));
    }

    @Test
    void shouldConvertStringValueWithQuotes() {
        // when
        Value value = converter.convert("'super mario?'");

        // then
        assertThat(value)
                .isInstanceOf(NonNullValue.class)
                .isEqualTo(new NonNullValue("super mario?"));
    }
}